package JunitDemo_CI.JunitDemo_CI;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

//test
public class calculatorTest {
	static calculadora calc;

	@BeforeAll
	public static void beforeAll()
	{
		calc = new calculadora();
		System.out.println("BeforeAll");
	}

	@AfterAll
	public static void afterAll(){
		System.out.println("AfterAll1");
	}

	@BeforeEach
	public void Before() {
		System.out.println("before()");
		
	}

	@Test	
	public void testSuma() {
		System.out.println("suma()");
		int resultado = calc.suma(2, 3);
		int esperado = 5;
		assertEquals(esperado, resultado);
		}
	@Test
	public void otherTest() {
		System.out.println("other test()");
		int a = calc.suma(2, 3);
		int expected = 6;
		assertEquals(expected,a);
	}
}